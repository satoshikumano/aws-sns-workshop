package com.example.aws_sns;

import java.io.IOException;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    public static final String TAG = "AWS-SNS"; 
    private static final String SENDER_ID = "715025018307";
    private TextView infoText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        infoText = (TextView) findViewById(R.id.infoText);
        infoText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "regID: " + ((TextView)v).getText());
            }
        });

        Button btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                registerGCM();
            }
        });
    }

    public void registerGCM() {
        new AsyncTask<Void, Void, Object>() {
            @Override
            protected void onPostExecute(Object result) {
                super.onPostExecute(result);
                if (result instanceof String) {
                    Log.v(TAG, "regID: " + result);
                    SharedPreferences prefs = MainActivity.this
                            .getSharedPreferences("GCM", Context.MODE_PRIVATE);
                    prefs.edit().putString("regId", (String) result).commit();
                    infoText.setText((CharSequence) result);
                } else if (result instanceof Exception) {
                    Exception e = (Exception) result;
                    infoText.setText(e.getMessage());
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Void... params) {
                SharedPreferences prefs = MainActivity.this
                        .getSharedPreferences("GCM", Context.MODE_PRIVATE);
                String storedReg = prefs.getString("regId", "");
                if (storedReg != "") {
                    return storedReg;
                }
                GoogleCloudMessaging gcm = GoogleCloudMessaging
                        .getInstance(MainActivity.this);
                try {
                    return gcm.register(SENDER_ID);
                } catch (IOException e) {
                    return e;
                }
            }
            
        }.execute(null,null,null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    
}
