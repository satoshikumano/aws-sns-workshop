package com.example.aws_sns;

import java.util.Iterator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class GcmBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle b = intent.getExtras();
        Iterator<String> itr = b.keySet().iterator();
        StringBuilder bld = new StringBuilder();
        for (b.keySet().iterator(); itr.hasNext() ;) {
            String key = itr.next();
            Object val = b.get(key);
            bld.append(key);
            bld.append(':');
            bld.append(val);
            bld.append('\n');
        }
        Toast.makeText(context, "Recieved message\n" + bld.toString(),
                Toast.LENGTH_LONG).show();
    }

}
